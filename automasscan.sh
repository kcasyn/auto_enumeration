#!/bin/bash

# written by @jthorpe6
masscanResolver(){
    for item in $(cat ./targets.ip);
    do
	if [ $(dig +short $item | wc -l) -eq '0' ];
	then
	    echo -e $item | awk '/[0-9]/' >> masscan/resolv.ip
	else
	    echo -e "$(dig +short $item | awk '/[0-9]/' | sort -u | tr -s ' ' '\n')" >> masscan/resolv.ip
	fi
#	echo -e "$(dig +short $item | awk '/[0-9]/' |  sort -u | tr -s ' ' '\n')" >> masscan/resolv.ip
    done
    
}

portscanallports(){
    masscan --open -iL masscan/resolv.ip \
	 -oG masscan/scans/portscanAll.gnmap -v \
	 -p 0-65535 \
	 --max-rate=$MAXRATE
}
