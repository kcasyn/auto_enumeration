#!/bin/bash

# written by jthorpe6

scan(){
    if [ $(cat open-ports/500.txt | wc -l) -eq '0' ];
    then
	echo -e "\n[${GREEN}+${RESET}] nothing for port ${YELLOW}500${RESET}"
    else
	nmap -sU -p 500 -iL open-ports/500.txt \
	     --script=ike-version -oN nse_scans/ike \
	     --stats-every 60s --min-hostgroup $MINHOST --min-rate=$MINRATE
	for ip in $(cat open-ports/500.txt)
	do	    
	    ike-scan -A -M $ip --id=GroupVPN | tee -a nse_scans/IKE-$ip.txt
	done
    fi

    if [ $(cat open-ports/443.txt | wc -l) -eq '0' ];
    then
	echo -e "\n[${GREEN}+${RESET}] nothing for port ${YELLOW}443${RESET}"
    else
	for ip in $(cat open-ports/443.txt)
	do
	    curl -v https://$ip/ -H "Host: hostname" \
		 -H "Range: bytes=0-18446744073709551615" -k | tee -a nse_scans/MS15034-$ip.txt
	done
    fi            
}
